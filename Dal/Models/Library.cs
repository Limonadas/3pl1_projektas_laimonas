﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.Models
{
    public class Library
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public int BookCount { get; set; }
        public Library(int id, string name, int count)
        {
            Id = id;
            Name = name;
            BookCount = count;
        }
    }
}
