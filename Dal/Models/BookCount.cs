﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.Models
{
    public class BookCount
    {
        public int BookId { get; set; }
        public string Title { get; set; }
        public int Count { get; set; }

        public BookCount (int B, string T, int C)
        {
            BookId = B;
            Title = T;
            Count = C;
        }
    }
}
