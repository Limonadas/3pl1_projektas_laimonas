﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.Models
{
    public class Author
    {
        public int? Id { get; set; }

        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public string Nationality { get; set; }

        public Author(int? id, string firstname, string lastname, string nationality)
        {
            Id = id;
            Firstname = firstname;
            Lastname = lastname;
            Nationality = nationality;
        }

        public Author(string firstname, string lastname, string nationality)
        {
            Firstname = firstname;
            Lastname = lastname;
            Nationality = nationality;
        }
    }
}
