﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Services;

namespace Presentation.Authors
{
    public partial class AuthorsList : Form
    {
        private readonly AuthorService _service; 
        public AuthorsList()
        {
            InitializeComponent();
            _service = new AuthorService();
            FillGrid();
        }

        ~AuthorsList()
        {
            _service.Dispose();
        }

        public void FillGrid()
        {
            var data = _service.GetAuthors();
        }
    }
}
