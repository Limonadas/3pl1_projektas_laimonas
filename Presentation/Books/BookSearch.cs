﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Services;
using Models = Dal.Models;

namespace Presentation.Books
{
    public partial class BookSearch : Form
    {
        private readonly BooksService _service;
        public BookSearch()
        {
            InitializeComponent();
            _service = new BooksService();
            FillGrid(new Models.BookSearch());
        }

        public void FillGrid(Models.BookSearch bookSearch)
        {
            var data = _service.SearchBooks(bookSearch);
        }

        private void BtnSearch_Click(object sender, EventArgs e)
        {
            var searchModel = new Models.BookSearch
            {
                Isbn = txtIsbn.Text,
                Title = txtTitle.Text,
                Author = txtAuthor.Text
            };

            FillGrid(searchModel);
        }

        private void Button1_Click(object sender, EventArgs e)
        {

        }
    }
}
