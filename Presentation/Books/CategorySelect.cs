﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Services;

namespace Presentation.Books
{
    public partial class CategorySelect : Form
    {
        private readonly CategoryService _service;
        public CategorySelect()
        {
            InitializeComponent();
            _service = new CategoryService();
            FillGrid();
        }

        public void FillGrid()
        {
            var categories = _service.GetCategories();

            foreach (var category in categories)
            {
                var rowIndex = dataGridView1.Rows.Add();
                dataGridView1.Rows[rowIndex].Cells["Id"].Value = category.Id.ToString();
                dataGridView1.Rows[rowIndex].Cells["CategoryName"].Value = category.Name;
            }
        }

        private void DataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                var checkbox = (DataGridViewCheckBoxCell)((DataGridView)sender).Rows[e.RowIndex].Cells[e.ColumnIndex];

                if (checkbox.Value == null) checkbox.Value = false;

                if ((bool)checkbox.Value == false)
                {
                    checkbox.Value = true;
                }
                else
                {
                    checkbox.Value = false;
                }

                EnableDisableSelectButton();
            }
        }

        private void EnableDisableSelectButton()
        {
            int count = 0;
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                if ((bool?)row.Cells["chxSelect"].Value == true)
                {
                    count++;
                }
            }

            if (count > 0)
            {
                btnSelect.Enabled = true;
            }
            else
            {
                btnSelect.Enabled = false;
            }
        }
    }
}
