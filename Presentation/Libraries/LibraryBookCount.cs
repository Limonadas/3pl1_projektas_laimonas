﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dal.Models;
using Services;

namespace Presentation.Libraries
{
    public partial class LibraryBookCount : Form
    {
        private readonly LibraryService _service;
        public LibraryBookCount()
        {
            InitializeComponent();
            _service = new LibraryService();
            FillGrid();
        }

        private void FillGrid()
        {
            var libraries = _service.GetLibraryBookCount();

            foreach (var library in libraries)
            {
                var rowIndex = dataGridView1.Rows.Add();
                dataGridView1.Rows[rowIndex].Cells["Id"].Value = library.Id.ToString();
                dataGridView1.Rows[rowIndex].Cells["LibraryName"].Value = library.Name;
                dataGridView1.Rows[rowIndex].Cells["BookCount"].Value = library.BookCount.ToString();
            }
        }

        private void DataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 1)
            {
                int libraryId = Convert.ToInt32(((DataGridView)sender).Rows[e.RowIndex].Cells["Id"].Value);
                string libraryName = ((DataGridView)sender).Rows[e.RowIndex].Cells["LibraryName"].Value.ToString();

                LibraryBooks bookCountForm= new LibraryBooks(libraryId,libraryName);
                bookCountForm.ShowDialog();
            }
        }
    }
}
