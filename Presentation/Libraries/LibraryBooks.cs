﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dal.Models;
using Services;

namespace Presentation.Libraries
{
    public partial class LibraryBooks : Form
    {
        private readonly LibraryService _service;
        public LibraryBooks(int libraryId, string libraryName)
        {
            InitializeComponent();
            label1.Text = libraryName;
            _service = new LibraryService();
            FillGrid(libraryId);
        }

        private void FillGrid(int libraryId)
        {
            var bookCounts = _service.GetLibraryBooks(libraryId);

            foreach (var book in bookCounts)
            {
                var rowIndex = dataGridView1.Rows.Add();
                dataGridView1.Rows[rowIndex].Cells["BookId"].Value = book.BookId.ToString();
                dataGridView1.Rows[rowIndex].Cells["Title"].Value = book.Title;
                dataGridView1.Rows[rowIndex].Cells["Count"].Value = book.Count.ToString();
            }
        }

        private void LibraryBooks_Load(object sender, EventArgs e)
        {

        }
    }
}
