﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dal.Models;
using MySql.Data.MySqlClient;

namespace Services
{
    public class LibraryService : BaseService, IDisposable
    {
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
        public List<Library> GetLibraryBookCount()
        {

            using (var connection = GetConnection())
            {
                var GetLibrariesCmdText = "Get_Library_Book_Count";
                var GetLibrariesCmd = new MySqlCommand(GetLibrariesCmdText, connection);
                GetLibrariesCmd.CommandType = System.Data.CommandType.StoredProcedure;

                MySqlDataAdapter adapter = new MySqlDataAdapter(GetLibrariesCmd);
                DataTable dataTable = new DataTable();
                adapter.Fill(dataTable);

                var list = dataTable.AsEnumerable().Select(m => new Library(m.Field<int>("LibraryId"), m.Field<string>("name"), m.Field<int>("count"))).ToList();

                return list;
            }
        }

        public List<BookCount> GetLibraryBooks(int inLibraryId)
        {

            using (var connection = GetConnection())
            {
                var GetLibrariesCmdText = "Get_Library_Books";
                var GetLibrariesCmd = new MySqlCommand(GetLibrariesCmdText, connection);
                GetLibrariesCmd.Parameters.AddWithValue("InLibraryId", inLibraryId);
                GetLibrariesCmd.CommandType = System.Data.CommandType.StoredProcedure;

                MySqlDataAdapter adapter = new MySqlDataAdapter(GetLibrariesCmd);
                DataTable dataTable = new DataTable();
                adapter.Fill(dataTable);

                var list = dataTable.AsEnumerable().Select(m => new BookCount(m.Field<int>("BookId"), m.Field<string>("Title"), m.Field<int>("bookCount"))).ToList();

                return list;
            }
        }
    }
}
