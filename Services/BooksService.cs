﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dal.Models;
using MySql.Data.MySqlClient;

namespace Services
{
    public class BooksService : BaseService
    {
        public List<Book> SearchBooks(BookSearch bookSearch)
        {
            using (var connection = GetConnection())
            {
                var searchBooksCmdText = "Filter_books";
                var searchBooksCmd = new MySqlCommand(searchBooksCmdText, connection);
                searchBooksCmd.CommandType = System.Data.CommandType.StoredProcedure;
                searchBooksCmd.Parameters.AddWithValue("InIsbn", string.IsNullOrWhiteSpace(bookSearch.Isbn) ? null : bookSearch.Isbn);
                searchBooksCmd.Parameters.AddWithValue("InTitle", string.IsNullOrWhiteSpace(bookSearch.Title) ? null : bookSearch.Title);
                searchBooksCmd.Parameters.AddWithValue("InAuthor", string.IsNullOrWhiteSpace(bookSearch.Author) ? null : bookSearch.Author);
                searchBooksCmd.Parameters.AddWithValue("InCategoryIds", "-1");

                MySqlDataAdapter adapter = new MySqlDataAdapter(searchBooksCmd);
                DataTable dataTable = new DataTable();
                adapter.Fill(dataTable);

                var list = dataTable.AsEnumerable().Select(m => new Book
                {
                    BookId = m.Field<int>("Id"),
                    Isbn = m.Field<string>("Isbn"),
                    Title = m.Field<string>("Title"),
                    DateOfPublish = m.Field<DateTime?>("DateOfPublish"),
                    Categories = m.Field<string>("Categories")
                }).ToList();

                return list;
            }
        }
    }
}
