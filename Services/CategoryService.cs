﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dal.Models;
using MySql.Data.MySqlClient;

namespace Services
{
    public class CategoryService : BaseService, IDisposable
    {
        public CategoryService()
        {

        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public List<Category> GetCategories()
        {

            var cmdText = "Get_Categories";

            using (var connection = GetConnection())
            {
                MySqlCommand getCategoriesCommand = new MySqlCommand(cmdText, connection);
                getCategoriesCommand.CommandType = System.Data.CommandType.StoredProcedure;
                //Parametrai

                //Duomenų ištraukimas ir apdorojimas
                MySqlDataAdapter adapter = new MySqlDataAdapter(getCategoriesCommand);

                DataTable dataTable = new DataTable();

                adapter.Fill(dataTable);

                var list = dataTable.AsEnumerable().Select(m => new Category
                (
                    m.Field<int?>("Id"), m.Field<string>("Name")
                )).ToList();

                //Duomenų grąžinimas
                return list;
            }


        }

        public void SaveCategory(Category category)
        {
            if (category.Id.HasValue)
            {
                try
                {
                    using (var connection = GetConnection())
                    {
                        var cmdText = "Update_Category";
                        MySqlCommand updateCategoryCommand = new MySqlCommand(cmdText, connection);
                        updateCategoryCommand.CommandType = CommandType.StoredProcedure;
                        updateCategoryCommand.Parameters.AddWithValue("InName", category.Name);
                        updateCategoryCommand.Parameters.AddWithValue("InId", category.Id);
                        connection.Open();

                        updateCategoryCommand.ExecuteNonQuery();

                        connection.Close();
                    }
                }
                catch (Exception ex)
                {
                    throw new InvalidOperationException(ex.Message.ToString());
                }
            } else
            {
                try
                {
                    using (var connection = GetConnection())
                    {
                        var cmdText = "Add_Category";
                        MySqlCommand addCategoryCommand = new MySqlCommand(cmdText, connection);
                        addCategoryCommand.CommandType = CommandType.StoredProcedure;
                        addCategoryCommand.Parameters.AddWithValue("InName", category.Name);
                        connection.Open();

                        addCategoryCommand.ExecuteNonQuery();

                        connection.Close();
                    }
                }
                catch (Exception ex)
                {
                    throw new InvalidOperationException(ex.Message.ToString());
                }
            }
            
        }

        public void Delete(int categoryId)
        {
            using (var connection = GetConnection())
            {
                var deleteCommandText = "Delete_Category";
                var deleteCommand = new MySqlCommand(deleteCommandText, connection);
                deleteCommand.CommandType = CommandType.StoredProcedure;
                deleteCommand.Parameters.AddWithValue("InCategoryId", categoryId);
                connection.Open();

                deleteCommand.ExecuteNonQuery();

                connection.Close();
            }
        }
    }
}
